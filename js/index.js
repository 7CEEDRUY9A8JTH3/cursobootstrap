$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({ interval: 2000 });
    $('#tipoPizza').on('show.bs.modal', function (e) {
        console.log('el modal se est� mostrando');
        $('#a�adirHaw').removeClass('btn-primary');
        $('#a�adirHaw').addClass('btn-success');
        $('#a�adirHaw').prop('disabled', true);
    });
    $('#tipoPizza').on('shown.bs.modal', function (e) { console.log('el modal se mostr�'); });
    $('#tipoPizza').on('hide.bs.modal', function (e) { console.log('el modal se est� ocultando'); });
    $('#tipoPizza').on('hidden.bs.modal', function (e) {
        console.log('el modal se ocult�');
        $('#a�adirHaw').removeClass('btn-success');
        $('#a�adirHaw').addClass('btn-primary');
        $('#a�adirHaw').prop('disabled', false);
    });
});